# Cypress Workshop

## Prerequisites

A code editor of choice, like Visual Studio Code
https://code.visualstudio.com/

ESLint is configured in this repo, so the ESLint extension is recommended.

Download and install Node.js

https://nodejs.org/en/download/

## installation

Checkout this repo and run npm install.

## running cypress interactively

npm test

## running cypress from cli

npm run test-cli

## linting sources

npm run lint

## Useful links

https://www.cypress.io/
https://github.com/cypress-io/eslint-plugin-cypress
