class ToDoPage {
  get newToDoInput(): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get('[data-test=new-todo]');
  }

  public enterToDo(item: string): void {
    this.newToDoInput.type(`${item}{enter}`);
  }
}

export default new ToDoPage();
